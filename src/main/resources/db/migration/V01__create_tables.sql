CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identification` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
SELECT * FROM credit_analysis_db.customer;

CREATE TABLE `interest_rate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tax` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `credit_analysis` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `credit_limit` decimal(19,2) DEFAULT NULL,
  `credit_limit_with_tax` decimal(19,2) DEFAULT NULL,
  `interest_amount` decimal(19,2) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `interest_rate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7veg274vb0mg6ttm93lnivtq7` (`customer_id`),
  KEY `FKb0xdxq3d7uq35tj8aag35ns7j` (`interest_rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


