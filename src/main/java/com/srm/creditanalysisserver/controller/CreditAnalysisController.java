package com.srm.creditanalysisserver.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.srm.creditanalysisserver.domain.CreditAnalysis;
import com.srm.creditanalysisserver.repository.CreditAnalysisRepository;
import com.srm.creditanalysisserver.repository.CreditAnalysisRepositoryQuery;
import com.srm.creditanalysisserver.service.CreditAnalysisService;

@RestController
@RequestMapping("credit-analysis-server")
public class CreditAnalysisController {
	
	@Autowired
	private CreditAnalysisService creditAnalysisService;
	@Autowired
	private CreditAnalysisRepository creditAnalysisRepository;
	@Autowired
	private CreditAnalysisRepositoryQuery creditAnalysisRepositoryQuery;
	

	@GetMapping("credit-analysis/customer/{customerId}")
    public ResponseEntity<List<CreditAnalysis>> listByCustomer(@PathVariable Long customerId) {
        List<CreditAnalysis> creditAnalysis = creditAnalysisRepositoryQuery.listByCustomer(customerId);
        return ResponseEntity.ok(creditAnalysis);
    }
	
	@PostMapping("credit-analysis")
	public ResponseEntity<CreditAnalysis> create(@Valid @RequestBody CreditAnalysis creditAnalysis) {
		CreditAnalysis savedCreditAnalysis = creditAnalysisService.save(creditAnalysis);
		return ResponseEntity.status(HttpStatus.CREATED).body(savedCreditAnalysis);
	}
	
	@DeleteMapping("credit-analysis/{creditAnalysisId}")
	public ResponseEntity<CreditAnalysis> delete(@PathVariable Long creditAnalysisId) {
		CreditAnalysis creditAnalysisDB = creditAnalysisRepository.findById(creditAnalysisId).get();
		creditAnalysisRepository.delete(creditAnalysisDB);
		return ResponseEntity.status(HttpStatus.OK).body(creditAnalysisDB);
	}
	
}
