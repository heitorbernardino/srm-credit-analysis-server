package com.srm.creditanalysisserver.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.srm.creditanalysisserver.domain.InterestRate;
import com.srm.creditanalysisserver.repository.InterestRateRepository;

@RestController
@RequestMapping("credit-analysis-server")
public class InterestRatesController {
	
	@Autowired
	private InterestRateRepository interestRateRepository;
	
	@GetMapping("interest-rates")
    public ResponseEntity<List<InterestRate>> list() {
        List<InterestRate> interestRates = interestRateRepository.findAll();
        return ResponseEntity.ok(interestRates);
    }
	
}
