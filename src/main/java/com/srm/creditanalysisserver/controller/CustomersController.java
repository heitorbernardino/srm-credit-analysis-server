package com.srm.creditanalysisserver.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.srm.creditanalysisserver.domain.Customer;
import com.srm.creditanalysisserver.repository.CustomerRepository;
import com.srm.creditanalysisserver.service.CustomerService;

@RestController
@RequestMapping("credit-analysis-server")
public class CustomersController {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CustomerService customerService;

	@GetMapping("customers")
    public ResponseEntity<List<Customer>> list() {
        List<Customer> customers = customerRepository.findAll();
        return ResponseEntity.ok(customers);
    }
	
	@PostMapping("customers")
	public ResponseEntity<Customer> create(@Valid @RequestBody Customer customer) {
		Customer savedCustomer = customerService.save(customer);
		return ResponseEntity.status(HttpStatus.CREATED).body(savedCustomer);
	}
	
	@GetMapping("customers/{id}")
    public ResponseEntity<Customer> getById(@PathVariable Long id) {
        Customer customer = customerRepository.findById(id).get();
        return ResponseEntity.ok(customer);
    }
}
