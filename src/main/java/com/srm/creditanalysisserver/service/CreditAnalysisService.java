package com.srm.creditanalysisserver.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.srm.creditanalysisserver.domain.CreditAnalysis;
import com.srm.creditanalysisserver.repository.CreditAnalysisRepository;

@Service
public class CreditAnalysisService {

	@Autowired
	private CreditAnalysisRepository creditAnalysisRepository;
	
	public CreditAnalysis save(CreditAnalysis creditAnalysis) {
		calculateTaxes(creditAnalysis);
		return creditAnalysisRepository.save(creditAnalysis);
	}
	
	public void calculateTaxes(CreditAnalysis creditAnalysis) {
        BigDecimal interestAmmount = creditAnalysis.getCreditLimit()
        		.multiply(creditAnalysis.getInterestRate().getTax()).setScale(2, RoundingMode.HALF_UP);
        BigDecimal creditLimmitAmmountWithTax = creditAnalysis.getCreditLimit()
        		.add(interestAmmount).setScale(2, RoundingMode.HALF_UP);

        creditAnalysis.setInterestAmmount(interestAmmount);
        creditAnalysis.setCreditLimmitWithTax(creditLimmitAmmountWithTax);
    }
}
