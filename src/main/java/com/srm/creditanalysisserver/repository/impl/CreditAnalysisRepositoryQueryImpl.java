package com.srm.creditanalysisserver.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.srm.creditanalysisserver.domain.CreditAnalysis;
import com.srm.creditanalysisserver.repository.CreditAnalysisRepositoryQuery;

@Repository
public class CreditAnalysisRepositoryQueryImpl implements CreditAnalysisRepositoryQuery {

	@PersistenceContext
    private EntityManager manager;
	
	@Override
	public List<CreditAnalysis> listByCustomer(Long id) {

        return manager
        	.createNamedQuery(CreditAnalysis.LIST_BY_CUSTOMER, CreditAnalysis.class)
        	.setParameter("customerId", id)
        	.getResultList();
	}

}
