package com.srm.creditanalysisserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.srm.creditanalysisserver.domain.CreditAnalysis;

public interface CreditAnalysisRepository extends JpaRepository<CreditAnalysis, Long>{
	
}
