package com.srm.creditanalysisserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.srm.creditanalysisserver.domain.InterestRate;

public interface InterestRateRepository extends JpaRepository<InterestRate, Long>{
	
}
