package com.srm.creditanalysisserver.repository;

import java.util.List;

import com.srm.creditanalysisserver.domain.CreditAnalysis;

public interface CreditAnalysisRepositoryQuery {
	List<CreditAnalysis> listByCustomer(Long id);
}
