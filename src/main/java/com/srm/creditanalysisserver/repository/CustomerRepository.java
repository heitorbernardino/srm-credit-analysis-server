package com.srm.creditanalysisserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.srm.creditanalysisserver.domain.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
	
}
