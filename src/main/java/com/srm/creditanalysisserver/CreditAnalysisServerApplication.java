package com.srm.creditanalysisserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreditAnalysisServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditAnalysisServerApplication.class, args);
	}
}
