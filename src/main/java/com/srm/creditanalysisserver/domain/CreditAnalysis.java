package com.srm.creditanalysisserver.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "credit_analysis")
@NamedQueries({
	@NamedQuery(name=CreditAnalysis.LIST_BY_CUSTOMER, query=CreditAnalysis.LIST_BY_CUSTOMER)
})
public class CreditAnalysis implements Serializable {

	private static final long serialVersionUID = -2307986201061333121L;
	
	public static final String LIST_BY_CUSTOMER = "select new CreditAnalysis(id, creditLimit, interestAmmount, active, "
			+ "creditLimmitWithTax, interestRate.id, interestRate.code, interestRate.description, interestRate.tax, customer.id) "
			+ "from CreditAnalysis where customer.id = :customerId";

	private Long id;
	private Customer customer;
	private InterestRate interestRate;
	private BigDecimal creditLimit = BigDecimal.ZERO;
	private BigDecimal interestAmmount = BigDecimal.ZERO;
	private BigDecimal creditLimmitWithTax = BigDecimal.ZERO;
	private boolean active = true;
	
	public CreditAnalysis() {
		
	}
	
	public CreditAnalysis(Long id, BigDecimal creditLimit, BigDecimal interestAmmount, boolean active, 
			BigDecimal creditLimmitWithTax, Long interestId, String interestRateCode, String interestRateDescription,
			BigDecimal interestRateTax, Long customerId) {
		this.id = id;
		this.creditLimit = creditLimit;
		this.interestAmmount = interestAmmount;
		this.creditLimmitWithTax = creditLimmitWithTax;
		this.active = active;
		this.interestRate = new InterestRate(interestId, interestRateCode, interestRateDescription, interestRateTax);
		this.customer = new Customer(customerId);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="customer_id")
	@NotNull(message="Informe um cliente.")
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	@ManyToOne
	@JoinColumn(name="interest_rate_id")
	@NotNull(message="Selecione a faixa de risco.")
	public InterestRate getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(InterestRate interestRate) {
		this.interestRate = interestRate;
	}

	@Column(name="credit_limit")
	@NotNull(message="Informe o limite de crédito")
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	@Column(name="interest_amount")
	public BigDecimal getInterestAmmount() {
		return interestAmmount;
	}

	public void setInterestAmmount(BigDecimal interestAmmount) {
		this.interestAmmount = interestAmmount;
	}

	@Column(name="credit_limit_with_tax")
	public BigDecimal getCreditLimmitWithTax() {
		return creditLimmitWithTax;
	}

	public void setCreditLimmitWithTax(BigDecimal creditLimmitWithTax) {
		this.creditLimmitWithTax = creditLimmitWithTax;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreditAnalysis other = (CreditAnalysis) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
