package com.srm.creditanalysisserver.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "interest_rate")
public class InterestRate implements Serializable {

	private static final long serialVersionUID = -7262565272044127219L;

	private Long id;
	private String code;
	private String description;
	private BigDecimal tax = BigDecimal.ZERO;
	
	public InterestRate() {
		
	}
	
	public InterestRate(Long id, String code, String description, BigDecimal tax) {
		this.id = id;
		this.code = code;
		this.description = description;
		this.tax = tax;
	}

	public InterestRate(String code, String description, BigDecimal tax) {
		this.code = code;
		this.description = description;
		this.tax = tax;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InterestRate other = (InterestRate) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
