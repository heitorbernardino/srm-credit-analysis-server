package com.srm.creditanalysisserver;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Before;
import org.junit.Test;

import com.srm.creditanalysisserver.domain.CreditAnalysis;
import com.srm.creditanalysisserver.domain.InterestRate;
import com.srm.creditanalysisserver.service.CreditAnalysisService;

public class CreditAnalysisTest {

	private CreditAnalysis creditAnalysis;
	private CreditAnalysisService creditAnalysisService;
	
	@Before
	public void before(){
		creditAnalysisService = new CreditAnalysisService();
		creditAnalysis = new CreditAnalysis();
		InterestRate interestRate = new InterestRate("B", "Faixa B", new BigDecimal("0.1"));
		creditAnalysis.setCreditLimit(new BigDecimal("1000"));
		creditAnalysis.setInterestRate(interestRate);
	}
	
	@Test
	public void shouldTestCalculateTaxes() {
		creditAnalysisService.calculateTaxes(creditAnalysis);
		
		BigDecimal creditLimmitAmmountWithTax = creditAnalysis.getCreditLimmitWithTax();
        BigDecimal expectedAmmount = new BigDecimal(1100).setScale(2, RoundingMode.HALF_UP);

        assertEquals(expectedAmmount, creditLimmitAmmountWithTax);
        
        BigDecimal interestAmount = creditAnalysis.getInterestAmmount();
        BigDecimal expectedInterestAmmount = new BigDecimal(100).setScale(2, RoundingMode.HALF_UP);

        assertEquals(expectedInterestAmmount, interestAmount);
	}

}
