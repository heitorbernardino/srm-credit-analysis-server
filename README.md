Sistema de Análise de crédito
==========================

Atualmente existe uma versão deste sistema rodando no seguinte endereço:
https://credit-analysis-hbsrm.herokuapp.com/

Instruções para rodar localmente.
==========================
- git clone https://heitorbernardino@bitbucket.org/heitorbernardino/srm-credit-analysis-server.git
- Instância banco de dados MYSQL rodando na porta padrão 3306, user (root) e password(12345) - 
	(Ou alterar as configurações de usuário e senha no arquivo resources/application.properties)
- mvn spring-boot:run

- Acessar o endereço http://localhost:8080

Tecnologias utilizadas
==========================
- Java 8 (https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- Spring Boot (https://spring.io/projects/spring-boot)
- Maven (https://maven.apache.org)
- MYSQL (https://www.mysql.com)
- FlywayDB (https://flywaydb.org)
- JUnit (https://junit.org)